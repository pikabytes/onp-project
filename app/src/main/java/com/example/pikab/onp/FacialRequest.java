package com.example.pikab.onp;

import android.util.Base64;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPDigestAuthFilter;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;


/**
 * Created by pikab on 10/14/2017.
 */


public class FacialRequest {


    public static String requestDB( String path, String nuDni) throws IOException {

        URI serviceLocation = UriBuilder.fromUri("http://devsvb.reniec.gob.pe/bioFacial/jaxrs-facial/").build();
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);

        String username = "userAppHACKIntegrated";
        String password = "/*dsf/d098q.";

        client.addFilter(new HTTPDigestAuthFilter(username, password));
        WebResource service = client.resource(serviceLocation);

        File fs = new File(path);
        java.io.FileInputStream fis = new java.io.FileInputStream(fs);
        byte[] bytesPhoto = new byte[(int) fs.length()];
        fis.read(bytesPhoto);
        fis.close();

        //String photoBase64 = Base64.encode(getPhotoBytes(path));
        String photoBase64 = Base64.encodeToString(bytesPhoto, Base64.DEFAULT);

        String nuRuc = "20295613620"; //dni del usuario quien realiza la captura
        String coUsuario = "40225629";//dni del ciudadano a quién se realiza la autenticación de identidad.
        nuDni = "71902975";
        String ipCliente = "10.10.10.10";
        String macCliente = "D4-C9-EF-D6-4E-0C";
        //Objeto para peticiones en formato JSON
        String input = "{\"nuRuc\":\"" + nuRuc + "\",\"coUsuario\":\"" + coUsuario + "\",\"nuDni\":\"" + nuDni + ""
                + "\",\"capturaFacial\":\"" + photoBase64 + "\",\"ipCliente\":\"" + ipCliente + "\",\"macCliente\":\"" + macCliente + "\"}";

        ClientResponse result = service.path("verificarIdentidadFacial").accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, input);
        //System.out.println("RESPONSE_IN_APPLICATION_JSON=" + result.getEntity(String.class));
        System.out.println("aca todo bien");
        String aaa = result.getEntity(String.class);
        return aaa;


    }


}




