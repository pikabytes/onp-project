package com.example.pikab.onp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SignOutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_out);

        TextView mIP = (TextView) findViewById(R.id.messageIp);
        String ip = getIntent().getStringExtra( "mip");
        String mac = getIntent().getStringExtra( "mmac");
        mIP.setText(ip+ " "+ mac);
    }
}
