package com.example.pikab.onp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.database.Cursor;
import android.content.Context;

import java.io.IOException;
import java.io.ByteArrayOutputStream;


public class MainActivity extends AppCompatActivity {

    private FacialItems fi = new FacialItems();
    private FacialRequest fr = new FacialRequest();
    private ImageView mphoto;
    private String m_path, m_dni;
    private TextView resPath;
    private Button login;
    private EditText DNI;
    private WebService ws = new WebService();

    Integer REQUEST_CAMERA = 1, SELECT_FILE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mphoto = (ImageView) findViewById(R.id.mphoto);
        resPath = (TextView) findViewById(R.id.resPath);
        login = (Button) findViewById(R.id.loginButton);
        DNI = (EditText) findViewById(R.id.dniText);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    m_dni = DNI.getText().toString();
                    if (mphoto.getDrawable() != null && !m_dni.matches("")) {
                        //getMACIP();
                        //String res = fr.requestDB(m_path, m_dni);
                        String res = ws.invokeHelloWorldWS("alexis","helloResponse");
                        System.out.println(res);

                        resPath.setText(res);

                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.takePhoto);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                SelectImage();
            }
        });


    }

    private void SelectImage(){

        final CharSequence [] items={"Camera","Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if(items[i].equals("Camera")){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult( intent, REQUEST_CAMERA);

                }else if(items[i].equals("Gallery")){
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    //startActivityForResult(intent, createChooser(intent, "Select File"), SELECT_FILE);
                    startActivityForResult(intent, SELECT_FILE);

                }else if(items[i].equals("Cancel")){
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){

            if(requestCode == REQUEST_CAMERA){
                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                mphoto.setImageBitmap(bmp);

                Uri  uri = getImageUri(getApplicationContext(), bmp);
                m_path = getRealPathFromURI(uri);
                resPath.setText(m_path);

            }else if(requestCode == SELECT_FILE){
                Uri uri = data.getData();
                //resPath.setText(uri.getPath());
                mphoto.setImageURI(uri);
                m_path = getRealPathFromURI(uri);
                resPath.setText(m_path);
            }

        }

    }

/*
    private void getMACIP(){
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        int ipAddress  = wm.getConnectionInfo().getIpAddress();
        m_ip = String.format(Locale.US,"%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
        m_mac = wm.getConnectionInfo().getMacAddress();
    }
*/

/*
    private String gettingAuthentication() throws Exception {
            return fr.requestDB(m_path, m_dni, m_ip, m_mac );
    }
*/

   /*
    private void Authenticate(){
        String dniText = DNI.getText().toString();
        if (mphoto.getDrawable()!=null && !dniText.matches("")){
            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            int ipAddress  = wm.getConnectionInfo().getIpAddress();
            String ip = String.format(Locale.US,"%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
            String mac = wm.getConnectionInfo().getMacAddress();

            Intent intent = new Intent(this, SignOutActivity.class);
            intent.putExtra( "mip", ip );
            intent.putExtra( "mmac", mac );
            startActivity(intent);
        }
    }
    */

    public String getRealPathFromURI(Uri contentUri)
    {
        try
        {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = managedQuery(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        catch (Exception e)
        {
            return contentUri.getPath();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

/*
    private void connectServer(){
        URI serviceLocation = UriBuilder.fromUri("http://devsvb.reniec.gob.pe/bioFacial/jaxrs-facial/").build();
        ClientConfig config =  new DefaultClientConfig();

    }
*/


}
