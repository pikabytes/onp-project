package com.example.pikab.onp;

/**
 * Created by pikab on 10/13/2017.
 */


public class FacialItems {
    String _ruc;
    String _userCode;
    String _dni;
    String _imgFacial;
    String _ip;
    String _mac;

    public String get_ruc(){
        return _ruc;
    }

    public  String get_userCode(){
        return _userCode;
    }

    public  String get_dni(){
        return _dni;
    }

    public String get_imgFacial(){
        return _imgFacial;
    }

    public String get_ip(){
        return _ip;
    }

    public String get_mac(){
        return  _mac;
    }

    public void set_ruc(String ruc){
        this._ruc = ruc;
    }

    public void set_userCode(String ucode){
        this._userCode = ucode;
    }

    public void set_dni(String dni){
        this._dni = dni;
    }

    public void set_imgFacial(String imgFacial){
        this._imgFacial = imgFacial;
    }

    public void set_ip(String ip){
        this._ip = ip;
    }

    public void set_mac(String mac){
        this._mac = mac;
    }




}
